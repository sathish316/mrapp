#!/usr/bin/sh

hadoop com.hlearn.mr.MaxTemperatureDriver \
    -conf /usr/local/conf/hadoop/hadoop-local.xml \
    -fs file:/// sample.txt output
