package com.hlearn.mr;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Test;

public class MaxTemperatureMapperTest {

    @Test
    public void processValidRecord() throws Exception {
        Text value = new Text("004301199099999" +
                "1950" + // year
                "051518004+68750+023550FM-12+038299999V0203201N00261220001CN9999999N9" +
                "-0011" + // temperature
                "1+99999999999");
        new MapDriver<LongWritable, Text, Text, IntWritable>()
                .withMapper(new MaxTemperatureMapper())
                .withInputValue(value)
                .withOutput(new Pair<Text, IntWritable>(new Text("1950"), new IntWritable(-11)))
                .runTest();
    }

    @Test
    public void ignoreMissingTemperatureRecord() throws Exception {
        Text value = new Text("004301199099999" +
                "1950" + // year
                "051518004+68750+023550FM-12+038299999V0203201N00261220001CN9999999N9" +
                "+9999" + // temperature
                "1+99999999999");
        new MapDriver<LongWritable, Text, Text, IntWritable>()
                .withMapper(new MaxTemperatureMapper())
                .withInputValue(value)
                .runTest();
    }
}
