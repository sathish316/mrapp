package com.hlearn.hbase;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Arrays;

public class ExampleClient {
    private Configuration config;
    private HBaseAdmin admin;

    public ExampleClient(Configuration config, HBaseAdmin admin) {
        this.config = config;
        this.admin = admin;
    }

    public static void main(String[] args) throws IOException, ServiceException {
        Configuration config = HBaseConfiguration.create();
        HBaseAdmin admin = new HBaseAdmin(config);
        System.out.println(admin.isMasterRunning());
        ExampleClient client = new ExampleClient(config, admin);

        HTableDescriptor table = client.createTable("test", "data");
        client.listTables();
        client.put("test", "row1", "data", "value1");
        client.put("test", "row2", "data", "value2");
        client.put("test", "row3", "data", "value3");
        client.get("test", "row1");
        client.scan("test");
        client.dropTable("test");
    }

    private void dropTable(String tableName) throws IOException {
        HTable table = new HTable(config, tableName);
        admin.disableTable(tableName);
        admin.deleteTable(tableName);
    }

    private void scan(String tableName) throws IOException {
        HTable table = new HTable(config, tableName);
        Scan scan = new Scan();
        ResultScanner scanner = table.getScanner(scan);
        try {
        for (Result scannerResult : scanner) {
            System.out.println("Scan:" + scannerResult);
        }
        } finally {
            scanner.close();
        }
    }

    private void get(String tableName, String rowName) throws IOException {
        HTable table = new HTable(config, tableName);
        byte[] row = Bytes.toBytes(rowName);
        Get g = new Get(row);
        Result result = table.get(g);
        System.out.println("Result:" + result);
    }

    private void put(String tableName, String row, String colFamily, String value) throws IOException {
        HTable table = new HTable(config, tableName);
        byte[] row1 = Bytes.toBytes(row);
        Put p1 = new Put(row1);
        p1.add(Bytes.toBytes(colFamily), Bytes.toBytes("1"), Bytes.toBytes(value));
        table.put(Arrays.asList(p1));
    }

    private void listTables() throws IOException {
        HTableDescriptor[] tables = admin.listTables();
        for(int i=0;i<tables.length;i++){
            System.out.println(tables[i].getName());
        }
    }

    private HTableDescriptor createTable(String tableName, String colFamilyName) throws IOException {
        HTableDescriptor htd = new HTableDescriptor(tableName);
        HColumnDescriptor hcd = new HColumnDescriptor(colFamilyName);
        htd.addFamily(hcd);
        admin.createTable(htd);
        return htd;
    }
}
