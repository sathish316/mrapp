package com.hlearn.hbasemr;

import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;

public class AnalyzeMapper extends TableMapper<Text, IntWritable> {

    enum Counters{
        ROWS,
        COLS,
        ERROR
    }
    private JSONParser parser = new JSONParser();

    @Override
    protected void map(ImmutableBytesWritable row, Result columns, Context context) throws IOException, InterruptedException {
        context.getCounter(Counters.ROWS).increment(1);
        String value = null;
        try {
            for (KeyValue kv: columns.list()) {
                context.getCounter(Counters.COLS).increment(1);
                value = Bytes.toStringBinary(kv.getValue());
                JSONObject json = (JSONObject) parser.parse(value);
                String author = (String) json.get("author");
                context.write(new Text(author), new IntWritable(1));
            }
        } catch (Exception e){
            context.getCounter(Counters.ERROR).increment(1);
        }
    }
}
