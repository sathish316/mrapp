package com.hlearn.hbasemr;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class FileImportMapper
        extends Mapper<LongWritable, Text, ImmutableBytesWritable, Writable> {

    private byte[] family = null;
    private byte[] qualifier = null;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        String column = context.getConfiguration().get("conf.column");
        byte[][] colkey = KeyValue.parseColumn(Bytes.toBytes(column));
        family = colkey[0];
        if(colkey.length > 1){
            qualifier = colkey[1];
        }
    }

    @Override
    protected void map(LongWritable offset, Text line, Context context) throws IOException, InterruptedException {
        try {
            String lineString = line.toString();
            byte[] rowkey = DigestUtils.md5(lineString);
            Put put = new Put(rowkey);
            put.add(family, qualifier, Bytes.toBytes(lineString));
            context.write(new ImmutableBytesWritable(rowkey), put);
            context.getCounter(FileImportDriver.Counters.LINES).increment(1);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
