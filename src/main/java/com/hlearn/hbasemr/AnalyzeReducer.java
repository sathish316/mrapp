package com.hlearn.hbasemr;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class AnalyzeReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    @Override
    protected void reduce(Text author, Iterable<IntWritable> articles, Context context) throws IOException, InterruptedException {
        int count = 0;
        for (IntWritable article : articles) {
            count++;
        }
        context.write(new Text(author), new IntWritable(count));
    }
}
