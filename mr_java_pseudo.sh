#!/usr/bin/sh
export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:target/mrapp-1.0-SNAPSHOT.jar

hadoop com.hlearn.mr.MaxTemperatureDriver \
  hdfs://localhost/sample.txt hdfs://localhost/output
